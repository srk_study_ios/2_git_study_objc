//
//  SongDetailCell.h
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *songNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;

@property (strong, nonatomic) NSString* artistName;
@property (strong, nonatomic) NSString* songName;
@property (strong, nonatomic) UIImage* artistIcon;


@end
