//
//  SongDetailCell.m
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import "SongDetailCell.h"

@implementation SongDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setSongName:(NSString *)songName {
    _songNameLabel.text = songName;
}

- (void)setArtistName:(NSString *)artistName {
    _artistNameLabel.text = artistName;
}

- (void)setArtistIcon:(UIImage *)artistIcon {
    _artistImageView.image = artistIcon;
}
@end
