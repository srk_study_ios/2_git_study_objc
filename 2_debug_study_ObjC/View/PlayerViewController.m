//
//  PlayerViewController.m
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import "PlayerViewController.h"


@interface PlayerViewController ()

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _titleStr;
    NSString *str = _previewUrl;
    NSURL *url = [NSURL URLWithString:str];
    self.player = [AVPlayer playerWithURL:url];
    NSLog(@"@%@", _titleStr);
    [self.player play];
}
@end
