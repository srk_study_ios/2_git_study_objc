//
//  SearchDetailViewController.m
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import "SearchDetailViewController.h"
#import "SongDetailCell.h"
#import "PlayerViewController.h"

@interface SearchDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SearchDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //これによってCell用に作成したxibファイルがUITableViewに登録される
    UINib *nib = [UINib nibWithNibName:@"SongDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SongDetailCell"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* CellIdentifier = @"SongDetailCell";
    
    SongDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.songName = self.items[indexPath.row][@"trackName"];
    NSLog(@"%@",cell.songName);
    cell.artistName = self.items[indexPath.row][@"artistName"];
    
    if (self.items[indexPath.row][@"artworkUrl100"]) {
        NSString *urlString = self.items[indexPath.row][@"artworkUrl100"];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        NSURLSessionDownloadTask *getImageTask = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            UIImage *downloadedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.artistIcon = downloadedImage;
            });
        }];
        [getImageTask resume];
    } else {
        cell.artistIcon = [UIImage imageNamed:@"noimage"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PlayerViewController* player = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerViewController"];
    player.titleStr = self.items[indexPath.row][@"trackName"];;
    player.previewUrl = self.items[indexPath.row][@"previewUrl"];
    [self.navigationController pushViewController:player animated:YES];
}
@end
