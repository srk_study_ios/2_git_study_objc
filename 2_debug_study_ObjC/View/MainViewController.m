//
//  MainViewController.m
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/05.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import "MainViewController.h"
#import "MBProgressHUD.h"
#import "SearchDetailViewController.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchSegmentControl;
@property (weak, nonatomic) IBOutlet UILabel *explanationText;
@property (strong, nonatomic) NSMutableData* recieveData;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)pushSearch:(id)sender {
    // itunesAPI
    NSString* str = @"";
    if (self.searchSegmentControl.selectedSegmentIndex == 0) {
        str = [NSString stringWithFormat:@"https://itunes.apple.com/search?term=%@&country=JP&lang=ja_jp&media=music&entity=song&attribute=artistTerm&limit=50", self.textField.text];
    } else {
        str = [NSString stringWithFormat:@"https://itunes.apple.com/search?term=%@&country=JP&lang=ja_jp&media=music&entity=song&attribute=songTerm&limit=50", self.textField.text];
    }
    
    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    NSURLSessionConfiguration* sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request];
    [task resume];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

// データ受け取り初期処理
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    NSLog(@"データ受け取り初期処理");
    self.recieveData = [[NSMutableData alloc] init];
    completionHandler(NSURLSessionResponseAllow);
}

// 受信の度に実行
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(nonnull NSData *)data {
    NSLog(@"受信の度に実行");
    [self.recieveData appendData:data];
}

// 完了
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    NSLog(@"通信完了");
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (error) {
        // Httpsリクエスト失敗
        dispatch_async(dispatch_get_main_queue(), ^{
            // 何かしらのせいで失敗
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"エラー"
                                                            message:@"通信エラーが発生しました。\n再度、ご確認ください。"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
        });
        
    } else {
        // Httpsリクエスト成功
        dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"リクエスト完了");
            // データ解析
            SearchDetailViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDetailViewController"];
            NSError* pError;
            id dict = [NSJSONSerialization JSONObjectWithData:self.recieveData options:NSJSONReadingMutableContainers error:&pError];
            vc.items = dict[@"results"];
            [self.navigationController pushViewController:vc animated:YES];
        });
    }
}

@end
