//
//  PlayerViewController.h
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface PlayerViewController : AVPlayerViewController

@property (strong, nonatomic) NSString* titleStr;
@property (strong, nonatomic) NSString* previewUrl;

@end

@interface AVPlayerView : UIView

@end
