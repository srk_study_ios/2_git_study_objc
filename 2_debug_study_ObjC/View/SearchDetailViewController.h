//
//  SearchDetailViewController.h
//  2_debug_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDetailViewController : UIViewController

@property (strong, nonatomic) NSArray* items;

@end
